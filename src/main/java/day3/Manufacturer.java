package day3;

import java.util.Objects;

public class Manufacturer {
    private String name;
    private int yearOfEstablishment;
    private String country;

    public Manufacturer(String name, int yearOfEstablishment, String country) {
        this.name = name;
        this.yearOfEstablishment = yearOfEstablishment;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public int getYearOfEstablishment() {
        return yearOfEstablishment;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public int hashCode(){
        return Objects.hash(name, country);
    }

    @Override
    public boolean equals(Object other){
        if(other == this){
            return true;
        }
        if(other instanceof Manufacturer){
            Manufacturer otherManufacturer = (Manufacturer)other;
            return name.equalsIgnoreCase(otherManufacturer.getName()) &&
                    country.equalsIgnoreCase(otherManufacturer.getCountry());
        }
        else {
            return false;
        }
    }
}
