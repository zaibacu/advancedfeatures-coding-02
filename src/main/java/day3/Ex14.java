package day3;

import java.util.*;

public class Ex14 {
    private static int NUMBERS_SIZE = 100000;
    private Random rnd = new Random();
    private List<Integer> numbers = new ArrayList<>(NUMBERS_SIZE);

    public Ex14(){
        initList();
    }

    public Ex14(int randomSeed){
        rnd = new Random(randomSeed);
        initList();
    }

    private void initList(){
        for(int i=0;i<NUMBERS_SIZE; i++){
            numbers.add(rnd.nextInt(1000));
        }
    }

    public List<Integer> getNumbers(){
        return numbers;
    }

    public Set<Integer> uniqueNumbers(){
        return new HashSet<>(numbers);
    }

    public Map<Integer, Integer> numberCounts(){
        Map<Integer, Integer> counter = new HashMap<>();
        for(Integer num : numbers){
            counter.put(num, counter.getOrDefault(num, 0) + 1);
        }

        return counter;
    }
}
