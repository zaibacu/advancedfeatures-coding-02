package day3;

import day2.Utils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Car {
    public enum EngineType {
        V12,
        V8,
        V6,
        S6,
        S4,
        S3
    }

    private String name;
    private String model;
    private int price;
    private int yearOfManufacture;
    private List<Manufacturer> manufacturers;
    private EngineType engineType;

    public Car(String name, String model, int price, int yearOfManufacture, List<Manufacturer> manufacturers, EngineType engineType) {
        this.name = name;
        this.model = model;
        this.price = price;
        this.yearOfManufacture = yearOfManufacture;
        this.manufacturers = manufacturers;
        this.engineType = engineType;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }

        if(o == null){
            return false;
        }

        if(o instanceof Car){
            Car other = (Car)o;
            int yearDelta = Math.abs(yearOfManufacture - other.getYearOfManufacture());

            Set<Manufacturer> h1 = new HashSet<>(manufacturers);
            Set<Manufacturer> h2 = new HashSet<>(other.getManufacturers());
            boolean sameManufacturer = Utils.intersection(h1, h2).size() > 0;

            return model.equalsIgnoreCase(other.getModel()) &&
                    yearDelta <= 3 &&
                    engineType == other.getEngineType() &&
                    sameManufacturer;

        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, model, engineType, yearOfManufacture);
    }

    @Override
    public String toString(){
        return String.format("<Car name=%s, model=%s, year=%d, engine=%s>", name, model, yearOfManufacture, engineType);
    }
}
