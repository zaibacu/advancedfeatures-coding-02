package day3;

public class MyRandom {
    private long seed;
    public MyRandom(){
        this(System.currentTimeMillis());
    }

    public MyRandom(long seed){
        this.seed = seed;
    }

    public int nextInt(int bound){
        seed = ((15678 * seed) + 15) % bound;
        return (int)seed;
    }
}
