package day3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CarService {
    private List<Car> cars = new ArrayList<>();

    public void addCar(Car c){
        cars.add(c);
    }
    public void removeCar(Car c){
        List<Integer> indicesToDelete = new ArrayList<>();
        int searchHashCode = c.hashCode();
        for(int i=0; i<cars.size(); i++){
            if(cars.get(i).hashCode() == searchHashCode){
                indicesToDelete.add(i);
            }
        }

        for(Integer idx : indicesToDelete){
            cars.remove(idx.intValue());
        }
    }

    public int carCount(){
        return cars.size();
    }

    public List<Car> getCars(){
        return cars;
    }

    public List<Car> getV12Cars(){
        return cars.stream()
                .filter(c -> c.getEngineType() == Car.EngineType.V12)
                .collect(Collectors.toList());
    }

    public enum SortDirection {
        ASC,
        DESC
    }

    public List<Car> sortedList(SortDirection direction){
        switch(direction){
            case ASC:
                return cars.stream().sorted(Comparator.comparingInt(Car::getYearOfManufacture)).collect(Collectors.toList());
            case DESC:
                return cars.stream().sorted(Comparator.comparingInt(Car::getYearOfManufacture).reversed()).collect(Collectors.toList());
            default:
                return Collections.EMPTY_LIST;
        }
    }

    public boolean carExists(Car other){
        int searchHashCode = other.hashCode();
        for(Car c : cars){
            if(c.hashCode() == searchHashCode){
                return true;
            }
        }

        return false;
    }

    public List<Car> carsByManufacturer(Manufacturer m){
        return cars.stream().filter(c -> c.getManufacturers().contains(m)).collect(Collectors.toList());
    }
}
