package day2;

public class Parcel{
    private int xLength;
    private int yLength;
    private int zLength;
    private float weight;
    private boolean isExpress;

    public Parcel(int xLength, int yLength, int zLength, float weight, boolean isExpress) {
        this.xLength = xLength;
        this.yLength = yLength;
        this.zLength = zLength;
        this.weight = weight;
        this.isExpress = isExpress;
    }

    public int getX() {
        return xLength;
    }

    public int getY() {
        return yLength;
    }

    public int getZ() {
        return zLength;
    }

    public float getWeight() {
        return weight;
    }

    public boolean isExpress() {
        return isExpress;
    }

    public int lengthSum(){
        return xLength + yLength + zLength;
    }

    public int minDimension(){
        return Utils.min(xLength, yLength, zLength);
    }
}
