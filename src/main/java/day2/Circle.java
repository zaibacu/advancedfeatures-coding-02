package day2;

import java.util.ArrayList;
import java.util.List;

public class Circle implements Movable, Resizable<Circle> {
    private double R;
    private Point2d center;
    private Point2d point;

    public Circle(Point2d center, Point2d point){
        this.R = center.distance(point);
        this.center = center;
        this.point = point;
    }

    public double getRadius(){
        return R;
    }

    public double getPerimeter(){
        return 2*Math.PI*R;
    }

    public double getArea(){
        return Math.PI * Math.pow(R, 2.0);
    }

    public List<Point2d> getSlicePoints(){
        List<Point2d> points = new ArrayList<>();
        double currentAngle = center.angle(point);

        for(int i=0; i<3; i++){
            currentAngle += 90;
            double y = center.getY() + Math.sin(currentAngle) * R;
            double x = center.getX() + Math.cos(currentAngle) * R;
            points.add(new Point2d(x, y));
        }

        return points;
    }

    @Override
    public void move(MoveDirection m) {
        center.move(m);
        point.move(m);
    }

    @Override
    public Circle resize(double factor) {
        double theta = center.angle(point);
        double newR = R * factor;
        double y = center.getY() + Math.sin(theta) * newR;
        double x = center.getX() + Math.cos(theta) * newR;

        Point2d newPoint = new Point2d(x, y);
        Circle newCircle = new Circle(center, newPoint);
        return newCircle;
    }
}
