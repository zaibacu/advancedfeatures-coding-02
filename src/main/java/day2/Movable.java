package day2;

public interface Movable {
    void move(MoveDirection m);
}
