package day2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ParcelValidator implements Validator<Parcel>{
    private List<String> errors = new ArrayList<>();

    @Override
    public boolean validate(Parcel input) {
        boolean isValid = true;
        if(input.lengthSum() > 300){
            errors.add(String.format("Length sum is too big. Max length sum is 300, your is: %d", input.lengthSum()));
            isValid = false;
        }

        if(input.minDimension() < 30){
            errors.add(String.format("Minimum dimension is too small. Limit is 30, your is: %d", input.minDimension()));
            isValid = false;
        }

        if(input.isExpress()){
            if(input.getWeight() > 15){
                errors.add(String.format("Your parcel is too heavy, limit for Express is 15, yours' is: %.2f", input.getWeight()));
                isValid = false;
            }
        }
        else {
            if(input.getWeight() > 30){
                errors.add(String.format("Your parcel is too heavy, limit for Normal is 30, yours' is: %.2f", input.getWeight()));
                isValid = false;
            }
        }

        return isValid;
    }

    @Override
    public Collection<String> getErrors() {
        return errors;
    }
}
