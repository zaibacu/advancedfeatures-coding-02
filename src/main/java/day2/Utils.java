package day2;

import java.util.Collection;
import java.util.Set;

public class Utils {
    public static int min(Integer... num){
        int m = 0;
        for(int i=0; i<num.length; i++){
            if(num[i] < num[m]){
                m = i;
            }
        }

        return num[m];
    }

    public static<T> Set<T> intersection(Set<T> s1, Set<T> s2){
        s1.retainAll(s2);
        return s1;
    }

    public static<T> void printList(Collection<T> lst){
        for(T item : lst){
            System.out.println(item);
        }
    }
}
