package day2;

public class Ex7 {
    private int limit;
    private int bulletsCount = 0;

    public Ex7(int limit) {
        this.limit = limit;
    }

    public void loadBullet(String bullet){
        for(Character c : bullet.toCharArray()){
            if(c != 'I'){
                continue;
            }

            bulletsCount++;

            if(bulletsCount >= limit){
                break;
            }
        }
    }

    public boolean isLoaded(){
        return bulletsCount > 0;
    }

    public void shot(){
        if(bulletsCount > 0){
            bulletsCount--;
            System.out.println("I");
        }
        else {
            System.err.println("Empty magazine");
        }
    }
}
