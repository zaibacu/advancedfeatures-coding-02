package day2;

public interface Resizable<T> {
    T resize(double factor);
}
