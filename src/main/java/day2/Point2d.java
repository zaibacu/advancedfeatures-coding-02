package day2;

public class Point2d implements Movable{
    private double x;
    private double y;

    public Point2d(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Point2d other){
        return Math.sqrt((Math.pow(x - other.getX(), 2.0) + Math.pow(y - other.getY(), 2.0)));
    }

    public double angle(Point2d other){
        return Math.toDegrees(Math.atan2(other.getY() - getY(), other.getX() - getX()));
    }

    @Override
    public void move(MoveDirection m) {
        this.x += m.getX();
        this.y += m.getY();
    }
}
