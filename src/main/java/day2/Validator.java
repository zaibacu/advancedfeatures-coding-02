package day2;

import java.util.Collection;

public interface Validator<T> {
    boolean validate(T input);

    Collection<String> getErrors();
}
