package day1;

import java.util.*;
import java.util.stream.Collectors;

public class Excercises {
    public static<T> void printList(Collection<T> vals){
        for(T s : vals){
            System.out.println(s);
        }
    }

    public static List<String> ex1(String... names){
        return ex1(Arrays.asList(names));
    }

    public static List<String> ex1(List<String> names){
        return names.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
    }

    public static List<String> ex2(List<String> names){
        return names.stream().sorted((a, b) -> b.toLowerCase().compareTo(a.toLowerCase())).collect(Collectors.toList());
    }

    public static int min(Integer[] numbers){
        return min(numbers, 0);
    }

    public static int min(Integer[] numbers, int offset){
        int m = offset;
        for(int i=offset; i<numbers.length; i++){
            if(numbers[i] < numbers[m]){
                m = i;
            }
        }

        return m;
    }

    public static int max(Integer[] numbers, int offset){
        int m = offset;
        for(int i=offset; i<numbers.length; i++){
            if(numbers[i] > numbers[m]){
                m = i;
            }
        }

        return m;
    }

    public static int max(Integer[] numbers){
        return max(numbers, 0);
    }

    public static void swap(Integer[] numbers, int a, int b){
        int temp = numbers[a];
        numbers[a] = numbers[b];
        numbers[b] = temp;
    }

    public static List<Integer> sort_minmax(Integer[] numbers){
        int opCount = 0;
        // MinMax, Bubble

        // x = | 5, 6 , 1, 4, 2
        // y = 1 | 5, 6, 4, 2
        // y' = 1 2 | 5, 6, 4
        // y'' = 1 2 4 | 5, 6 ...

        for(int i=0; i<numbers.length; i++){
            int m = max(numbers, i);
            swap(numbers, i, m);
            opCount++;
        }

        System.out.println("Op Count: " + opCount);
        return Arrays.asList(numbers);
    }

    public static List<Integer> sort_bubble(Integer[] numbers){
        int opCount = 0;
        while(true){
            boolean changed = false;

            for(int i=0; i<numbers.length - 1; i++){
                opCount++;
                if(numbers[i] > numbers[i+1]){
                    changed = true;
                    swap(numbers, i, i+1);
                }
            }

            if(!changed){
                break;
            }
        }

        System.out.println("Op Count: " + opCount);
        return Arrays.asList(numbers);
    }

    public static void ex3(Map<String, Integer> values){
        /*for(Map.Entry<String, Integer> e : values.entrySet()){
            System.out.printf("Key: %s, Value: %d\n", e.getKey(), e.getValue());
        }*/

        /*
        Object[] entries = values.entrySet().toArray();
        for(int i=0; i<entries.length; i++){
            Map.Entry<String, Integer> e = (Map.Entry<String, Integer>)entries[i];
            String delim = ",";
            if(i == entries.length - 1){
                delim = ".";
            }
            System.out.printf("Key: %s, Value: %d%s\n", e.getKey(), e.getValue(), delim);
        }*/

        String result = values
                .entrySet()
                .stream()
                .map(e -> String.format("Key: %s, Value: %d", e.getKey(), e.getValue()))
                .collect(Collectors.joining(",\n"));
        System.out.println(result + ".");
    }

    public static void ex6(Map<String, String> m){
        List<Map.Entry<String, String>> entries = new ArrayList(m.entrySet());

        System.out.printf("First: %s\n", entries.get(0));
        System.out.printf("Last: %s\n", entries.get(entries.size() - 1));
    }

    public static void main(String[] args){
        // A-Z a-z
        List<String> names = Arrays.asList("Zenonas", "Ona", "ona", "Juozas", "Juozas");
        printList(ex1(names));
        System.out.println("--------------------");
        printList(ex2(names));

        Map<String, Integer> nameCounts = new CounterMap<>(names);
        System.out.println(nameCounts.get("Juozas"));

        Map<Integer, Integer> numberCounts = new CounterMap<Integer>(1, 1, 2, 3, 5, 8, 13);

        System.out.println("------Ex3------");
        Map<String, Integer> values = new DefaultMap<>();
        values.put("Java", 18);
        values.put("Python", 1);
        values.put("PHP", 0);

        ex3(values);

        System.out.println("------Ex4------");
        Storage storage = new Storage();
        storage.addToStorage("test", "1");
        storage.addToStorage("test", "2");
        storage.addToStorage("test2", "2");
        storage.addToStorage("test3", "2");


        storage.printValues("non-existing");
        storage.printValues("test");

        storage.findValues("1");
        storage.findValues("2");

        System.out.println("------Ex5------");
        System.out.println("Initial");
        Set<Integer> mySet = new HashSet<>();
        mySet.add(1);
        mySet.add(1);
        mySet.add(1);
        mySet.add(2);

        printList(mySet);

        System.out.println("Custom");
        Set<Integer> myOtherSet = new SDAHashSet<>();
        myOtherSet.add(1);
        myOtherSet.add(1);
        myOtherSet.add(1);
        myOtherSet.add(2);

        printList(myOtherSet);

        System.out.println("------Ex6------");
        Map<String, String> ourValues = new TreeMap<>(Comparator.reverseOrder());
        ourValues.put("Albinas", "1");
        ourValues.put("Bronius", "2");
        ourValues.put("Ceslovas", "3");
        ourValues.put("Darius", "4");
        ourValues.put("Erikas", "5");
        ex6(ourValues);
    }
}
