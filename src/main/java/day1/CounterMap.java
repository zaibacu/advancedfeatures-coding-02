package day1;

import java.util.*;

public class CounterMap<T> implements Map<T, Integer> {
    private Map<T, Integer> counts = new HashMap<>();

    public CounterMap(T... values){
        this(Arrays.asList(values));
    }

    public CounterMap(List<T> values){
        for(T v : values){
            int cnt = counts.getOrDefault(v, 0);
            counts.put(v, cnt + 1);
        }
    }

    @Override
    public int size() {
        return counts.size();
    }

    @Override
    public boolean isEmpty() {
        return counts.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return counts.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public Integer get(Object key) {
        return counts.get(key);
    }

    @Override
    public Integer put(T key, Integer value) {
        return null;
    }

    @Override
    public Integer remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends T, ? extends Integer> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<T> keySet() {
        return counts.keySet();
    }

    @Override
    public Collection<Integer> values() {
        return null;
    }

    @Override
    public Set<Entry<T, Integer>> entrySet() {
        return counts.entrySet();
    }
    // N - reikšmių
    // Noriu gauti kiek yra atsikartojimų
}
