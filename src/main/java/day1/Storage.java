package day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Storage {
    private Map<String, List<String>> data = new DefaultMap<>();

    public void addToStorage(String key, String value){
        data.getOrDefault(key, new ArrayList<>()).add(value);
    }

    public void printValues(String key){
        if(!data.containsKey(key)){
            System.err.printf("Key '%s' doesn't exist\n", key);
            return;
        }

        for(String v : data.get(key)){
            System.out.println(v);
        }
    }

    public void findValues(String value){
        for(Map.Entry<String, List<String>> e : data.entrySet()){
            for(String v : e.getValue()){
                if(v.equalsIgnoreCase(value)){
                    System.out.printf("Key: '%s' contains value: '%s'\n", e.getKey(), v);
                }
            }
        }
    }
}
