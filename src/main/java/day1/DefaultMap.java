package day1;

import java.util.HashMap;
import java.util.Map;

public class DefaultMap<T, U> extends HashMap<T, U> implements Map<T, U> {

    @Override
    public U getOrDefault(Object key, U defaultValue){
        if(!this.containsKey(key)){
            this.put((T)key, defaultValue);
        }

        return this.get(key);
    }
}
