package day2;

import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class Day2Tests {
    @Test
    public void testEx7(){
        Ex7 e = new Ex7(10);

        e.loadBullet("III");

        assertTrue(e.isLoaded());

        for(int i=0; i<4; i++){
            e.shot();
        }

        assertFalse(e.isLoaded());
    }

    @Test
    public void testMin(){
        int result = Utils.min(3, 2, 11, 5, 10, 15);
        assertEquals(2, result);
    }

    @Test
    public void testParcelOk(){
        Validator v = new ParcelValidator();
        Parcel p = new Parcel(30, 30, 30, 10, false);

        assertTrue(v.validate(p));
    }

    @Test
    public void testParcelTooSmall(){
        Validator v = new ParcelValidator();
        Parcel p = new Parcel(30, 25, 30, 10, false);

        assertFalse(v.validate(p));

        Collection<String> errors = v.getErrors();
        assertEquals(1, errors.size());
        assertEquals("Minimum dimension is too small. Limit is 30, your is: 25", errors.toArray()[0]);
    }

    @Test
    public void testParcelTooHeavy(){
        Validator v = new ParcelValidator();
        Parcel p = new Parcel(30, 30, 30, 35, false);

        assertFalse(v.validate(p));

        Collection<String> errors = v.getErrors();
        assertEquals(1, errors.size());
        //assertEquals("Your parcel is too heavy, limit for Normal is 30, yours' is: 35,00", errors.toArray()[0]);
    }

    @Test
    public void testParcelExpressTooHeavy(){
        Validator v = new ParcelValidator();
        Parcel p = new Parcel(30, 30, 30, 16, true);

        assertFalse(v.validate(p));

        Collection<String> errors = v.getErrors();
        assertEquals(1, errors.size());
        //assertEquals("Your parcel is too heavy, limit for Express is 15, yours' is: 16,00", errors.toArray()[0]);
    }

    @Test
    public void testParcelMultipleErrors(){
        Validator v = new ParcelValidator();
        Parcel p = new Parcel(30, 15, 30, 16, true);

        assertFalse(v.validate(p));

        Collection<String> errors = v.getErrors();
        assertEquals(2, errors.size());
        //assertEquals("Minimum dimension is too small. Limit is 30, your is: 15", errors.toArray()[0]);
        //("Your parcel is too heavy, limit for Express is 15, yours' is: 16,00", errors.toArray()[1]);
    }

    @Test
    public void testCircle(){
        Point2d p1 = new Point2d(5, 5);
        Point2d p2 = new Point2d(2, 3);

        assertEquals(p1.distance(p2), p2.distance(p1), 0.01);
        assertEquals(3.60, p1.distance(p2), 0.01);

        Circle c = new Circle(p2, p1);

        assertEquals(3.60, c.getRadius(), 0.01);
        assertEquals(22.654, c.getPerimeter(), 0.01);
        assertEquals(40.84, c.getArea(), 0.01);

        List<Point2d> results = c.getSlicePoints();

        assertEquals(3, results.size());
    }

    @Test
    public void testMovePoint(){
        Point2d p = new Point2d(5, 5);
        p.move(new MoveDirection(1, 2));

        assertEquals(6, p.getX(), 0.01);
        assertEquals(7, p.getY(), 0.01);
    }

    @Test
    public void testMoveCircle(){
        Point2d p1 = new Point2d(5, 5);
        Point2d p2 = new Point2d(2, 3);

        Circle c = new Circle(p2, p1);

        c.move(new MoveDirection(1, 1));
        assertEquals(new Point2d(3, 4).distance(new Point2d(6, 6)), c.getRadius(), 0.01);

        List<Point2d> results = c.getSlicePoints();
    }

    @Test
    public void testResizeCircle(){
        Point2d p1 = new Point2d(5, 5);
        Point2d p2 = new Point2d(2, 3);

        Circle c = new Circle(p2, p1);
        Circle smallerCircle = c.resize(0.5);
        assertEquals(c.getRadius(), smallerCircle.getRadius() * 2, 0.01);
    }
}
