package day3;

import day1.CounterMap;
import day2.Utils;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static junit.framework.TestCase.*;

public class Day3Tests {
    @Test
    public void testUnion(){
        Set<Integer> s1 = new HashSet<>();
        Set<Integer> s2 = new HashSet<>();
        Set<Integer> expected = new HashSet<>();

        expected.add(2);

        s1.add(1);
        s1.add(2);

        s2.add(2);
        s2.add(3);

        assertEquals(expected, Utils.intersection(s1, s2));
    }

    @Test
    public void testHashCodes(){
        Manufacturer m1 = new Manufacturer("test", 2021, "test");
        Manufacturer m2 = new Manufacturer("test", 2020, "test");

        int h1 = m1.hashCode();
        int h2 = m2.hashCode();

        assertEquals(h1, h2);
        assertTrue(m1 == m1);
        assertTrue(m1.equals(m1));
        assertTrue(m1.equals(m2));
        //assertTrue(m1 == m2);
    }

    @Test
    public void testCarsEqual(){
        List<Manufacturer> m1 = new ArrayList<>();
        List<Manufacturer> m2 = new ArrayList<>();

        m1.add(new Manufacturer("Audi", 1965, "Germany"));
        m1.add(new Manufacturer("VW", 1965, "Germany"));

        m2.add(new Manufacturer("VW", 1965, "Germany"));

        Car c1 = new Car("LLL001", "A4", 3000, 2006, m1, Car.EngineType.S4);
        Car c2 = new Car("LSL051", "A4", 4500, 2004, m2, Car.EngineType.S4);

        assertTrue(c1.equals(c2));
    }

    @Test
    public void testCarService(){
        List<Manufacturer> m1 = new ArrayList<>();
        List<Manufacturer> m2 = new ArrayList<>();
        List<Manufacturer> m3 = new ArrayList<>();

        m1.add(new Manufacturer("Audi", 1965, "Germany"));
        m1.add(new Manufacturer("VW", 1965, "Germany"));
        m1.add(new Manufacturer("Seat", 1950, "Spain"));

        m2.add(new Manufacturer("VW", 1965, "Germany"));

        m3.add(new Manufacturer("Lamborghini", 1963, "Italy"));

        Car c1 = new Car("LLL001", "A4", 3000, 2006, m1, Car.EngineType.S4);
        Car c2 = new Car("LSL051", "A4", 4500, 2004, m2, Car.EngineType.S4);
        Car c3 = new Car("LSO151", "A7", 14500, 2012, m1, Car.EngineType.V12);
        Car c4 = new Car("GOD100", "Miura", 145000, 1978, m3, Car.EngineType.V12);

        CarService carService = new CarService();
        // 1.
        carService.addCar(c1);
        carService.addCar(c2);
        assertEquals(2, carService.carCount());

        // 2.
        carService.removeCar(c2);
        assertEquals(1, carService.carCount());

        // 3.
        List<Car> r1 = carService.getCars();
        assertEquals(1, r1.size());

        // 4.
        carService.addCar(c3);
        List<Car> r2 = carService.getV12Cars();
        assertEquals(1, r2.size());
        assertEquals(c3, r2.get(0));

        // 5.
        carService.addCar(c4);
        List<Car> r3 = carService.getCars()
                .stream()
                .filter(c -> c.getYearOfManufacture() < 1999)
                .collect(Collectors.toList());

        assertEquals(1, r3.size());
        assertEquals(c4, r3.get(0));

        // 6.
        Optional<Car> mostExpensiveCar = carService.getCars().stream().max(Comparator.comparingInt(Car::getPrice));
        assertTrue(mostExpensiveCar.isPresent());
        assertEquals(c4, mostExpensiveCar.get());

        // 7.
        Optional<Car> cheapestCar = carService.getCars().stream().min(Comparator.comparingInt(Car::getPrice));
        // Optional<Car> cheapestCar = carService.getCars().stream().min((o1, o2) -> Integer.compare(o1.getPrice(), o2.getPrice()));

        assertTrue(cheapestCar.isPresent());
        assertEquals(c1, cheapestCar.get());

        // 8.
        List<Car> r4 = carService.getCars().stream().filter(c -> c.getManufacturers().size() >= 3).collect(Collectors.toList());
        assertEquals(2, r4.size());

        // 9.
        List<Car> r5 = carService.sortedList(CarService.SortDirection.ASC);
        List<Car> r6 = carService.sortedList(CarService.SortDirection.DESC);

        assertEquals(c4, r5.get(0));
        assertEquals(c3, r6.get(0));

        // 10.
        assertFalse(carService.carExists(c2));

        // 11.
        List<Car> r7 = carService.carsByManufacturer(new Manufacturer("VW", 1965, "Germany"));
        assertEquals(2, r7.size());

        // 12.
        List<Car> r8 = carService.getCars().stream().filter(c -> {
            return c.getManufacturers().stream().filter(m -> m.getYearOfEstablishment() < 1960).count() > 0;
        }).collect(Collectors.toList());

        assertEquals(2, r8.size());
        Utils.printList(r8);
    }

    @Test
    public void testRandomNumbers(){
        Ex14 ex14 = new Ex14(42);

        assertEquals(1000, ex14.uniqueNumbers().size());

        Map<Integer, Integer> results = ex14.numberCounts();

        List<Integer> top25 = results
                .entrySet()
                .stream()
                .sorted((o1, o2) -> -Integer.compare(o1.getValue(), o2.getValue()))
                .mapToInt(n -> n.getValue())
                .limit(25)
                .boxed()
                .collect(Collectors.toList());

        assertEquals(136, top25.get(0).intValue());
    }

    @Test
    public void testCustomRandom(){
        MyRandom rnd = new MyRandom();
        for(int i=0; i<100; i++) {
            System.out.println(rnd.nextInt(100));
        }
    }
}
